# IQUAview tutorials

This repository contains tutorials to learn about [IQUAview](https://bitbucket.org/iquarobotics/iquaview).

[TOC]

## Add new plugin

In this tutorial you will learn how to add new plugin to IQUAview ([link](add_new_plugin/README.md)).

## Useful helpers for plugin development

### Accessing other plugins from your plugin

Need to wait for all plugins to be loaded. Then we can, for example, connect to their signals.

```python
import logging
from iquaview_lib.baseclasses.pluginbase import PluginBase
from iquaview_lib.utils.qt_signal import reconnect_signal_to_slot

class CustomModule(PluginBase):
    def __init__(self, **kwargs) -> None:
        self.parent().plugin_manager.plugins_loaded_signal.connect(
            self.callback_plugins_loaded
        )

    def callback_plugins_loaded(self) -> None:
        # custom messages
        evologics_plugin = self.parent().plugin_manager.get_plugin(
            "iquaview_evologics_usbl"
        )
        if evologics_plugin is not None:
            reconnect_signal_to_slot(
                signal=evologics_plugin.custom_message_output_signal,
                callback=self.callback_custom_message,
            )
            logging.info("connected to evologics plugin custom message")
        else:
            logging.fatal("cannot connect to evologics plugin custom message")

    def callback_custom_message(self, custom: bytes) -> None:
        pass
```

### Add layers to the project

Need to wait for the full project to load.
(might fail until project is correctly saved, on new project: save and reopen)

```python
from iquaview_lib.baseclasses.pluginbase import PluginBase
from qgis.core import edit, QgsProject, QgsVectorLayer, QgsLayerTreeLayer, QgsGeometry, QgsPointXY, QgsFeature, QgsLayerTreeGroup

GROUP = "MyGroup"
LAYER = {
    "name": "my_layer_name",
    "description": "point?crs=epsg:4326",
}

class CustomModule(PluginBase):
    def __init__(self, **kwargs) -> None:
        # on load
        self.parent().project_loaded_signal.connect(self.setup_layers)
        # on save
        QgsProject.instance().projectSaved.connect(self.setup_layers)
        # layer definition
        self.group_layer = QgsLayerTreeGroup(GROUP)
        self.layer: Optional[QgsVectorLayer] = None

    def setup_layers(self) -> None:
        project = QgsProject.instance()
        # add group if not present
        group = project.layerTreeRoot().findGroup(GROUP)
        if group is None:
            project.layerTreeRoot().addChildNode(self.group_layer)
        else:
            self.group_layer = group
        # add layer if not present
        layers = project.mapLayersByName(self._layer_name)
        if layers:
            self.layer = layers[0]
        else:
            self.layer = QgsVectorLayer(
                LAYER["description"],
                LAYER["name"],
                "memory",
            )
            project.addMapLayer(self.layer, False)
            group.addChildNode(QgsLayerTreeLayer(self.layer))

    def draw_point(self. lat: float, lon: float) -> None:
        if self.layer is None:
            return
        with edit(self.layer):
            g = QgsGeometry.fromPointXY(QgsPointXY(lon, lat))
            feature = QgsFeature(self.layer.fields())
            feature.setGeometry(g)
            self.layer.addFeature(feature)
```
