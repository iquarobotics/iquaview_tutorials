# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [1.0.0] - 2020-10-23

### Added

- Lorem ipsum dolor sit amet, consectetur adipiscing elit.
- Nam at enim sed quam ultricies elementum et sed lacus.

### Changed

- Lorem ipsum dolor sit amet, consectetur adipiscing elit.

### Removed

- Lorem ipsum dolor sit amet, consectetur adipiscing elit.

