#!/usr/bin/env python3

from iquaview_example_plugin import PLUGIN_NAME, __version__
from setuptools import find_packages, setup

setup(
    name=PLUGIN_NAME,
    version=__version__,
    description="IQUAview example plugin",
    author="Iqua Robotics",
    author_email="software@iquarobotics.com",
    packages=find_packages(exclude="tests"),
    include_package_data=True,
    package_data={PLUGIN_NAME: ["*.xml", "py.typed"]},
)
