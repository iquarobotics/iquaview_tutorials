# IQUAview Example Plugin

[TOC]

## Install

To install Python packages to the system, go to the iquaview_example_plugin folder and type:

```bash
pip3 install .
```

## Uninstall

To uninstall the package:

```bash
pip3 uninstall iquaview_example_plugin
```
