# Copyright (c) 2022 Iqua Robotics SL - All Rights Reserved
#
# This file is subject to the terms and conditions defined in file
# 'LICENSE.txt', which is part of this source code package.

"""
Example plugin Dialog
"""

# =============================================================================
# Stdlib imports
# =============================================================================
import logging
import os
from typing import Dict, Optional

# ==============================================================================
# iquaview_lib imports
# ==============================================================================
from iquaview_lib.cola2api.cola2_interface import SubscribeToTopic, get_ros_param, send_trigger_service
from iquaview_lib.definitions import ROSBRIDGE_SERVER_PORT
from iquaview_lib.utils.workerthread import Worker
from iquaview_lib.vehicle.auvconfigparams import AUVConfigParamsDlg
from iquaview_lib.vehicle.vehicleinfo import VehicleInfo
from iquaview_lib.xmlconfighandler.vehicledatahandler import VehicleDataHandler

# ==============================================================================
# Qt imports
# ==============================================================================
from PyQt5.QtCore import QThread, QThreadPool, QTimer, pyqtSignal, pyqtSlot
from PyQt5.QtGui import QCloseEvent, QIcon
from PyQt5.QtWidgets import QAction, QDialog, QMessageBox, QWidget

# ==============================================================================
# Local imports
# ==============================================================================
from iquaview_example_plugin.ui.ui_plugin_example import Ui_PluginExample

logger = logging.getLogger(__name__)


class ExamplePluginDlg(QDialog, Ui_PluginExample):
    """Example plugin dialog"""

    update_data_signal = pyqtSignal()
    reconnect_signal = pyqtSignal()

    def __init__(self, vehicle_info: VehicleInfo, parent: Optional[QWidget] = None) -> None:
        # Init parent
        super().__init__(parent)
        self.setupUi(self)

        # Init variables
        self.connected: bool = False
        self.config_name: str = "example_plugin_configuration.xml"
        self.configfile: str = os.path.join(os.path.dirname(os.path.abspath(__file__)), self.config_name)
        self.elapsed_time_topic: Optional[SubscribeToTopic] = None
        self.subscribed: bool = False
        self.timer = QTimer()
        self.timeout: int = 0
        self.time: int = 0
        self.threadpool = QThreadPool()
        self.vehicle_info = vehicle_info

        # load configuration
        self.topic_names: Dict[str, str] = {}
        self.services_names: Dict[str, str] = {}
        self.load_configuration()

        # add actions to toolbar
        self.configuration_action = QAction(
            QIcon(":/resources/mActionAUVConfigParameters.svg"),
            "Config Parameters",
            self,
        )
        self.toolbar.addAction(self.configuration_action)

        # connect signals to slots
        self.configuration_action.triggered.connect(self.open_configuration)
        self.reset_pushButton.clicked.connect(self.reset_timeout)
        self.timer.timeout.connect(self.refresh_timeout)
        self.update_data_signal.connect(self.start_timer)
        self.reconnect_signal.connect(self.connect)

    def load_configuration(self) -> None:
        # create vehicle data handler
        vd_handler = VehicleDataHandler(self.configfile)

        # get vehicle data topics from xml
        xml_vehicle_data_topics = vd_handler.read_topics()
        for topic in xml_vehicle_data_topics:
            self.topic_names[topic.get("id")] = topic.text

        # get vehicle data services from xml
        xml_vehicle_data_services = vd_handler.read_services()
        for service in xml_vehicle_data_services:
            self.services_names[service.get("id")] = service.text

    @pyqtSlot()
    def open_configuration(self) -> None:
        """Open configuration"""
        acpd = AUVConfigParamsDlg(config_filename=self.configfile, vehicle_info=self.vehicle_info, unique_section=True)
        try:
            acpd.load_section("Safety")
            acpd.exec_()

        except Exception as ex:
            logger.error("Connection with COLA2 could not be established")
            QMessageBox.critical(
                self,
                "Connection with AUV Failed",
                f"{ex} Connection with COLA2 could not be established",
                QMessageBox.Close,
            )

    @pyqtSlot()
    def reset_timeout(self) -> None:
        """Send service that reset timeout"""
        if self.connected:
            reset_timeout = self.services_names.get("reset timeout")
            if reset_timeout is not None:
                response = send_trigger_service(
                    self.vehicle_info.get_vehicle_ip(),
                    ROSBRIDGE_SERVER_PORT,
                    self.vehicle_info.get_vehicle_namespace() + reset_timeout,
                )
                if not response["values"]["success"]:
                    QMessageBox.critical(self, "Reset timeout failed", response["values"]["message"], QMessageBox.Close)
                self.set_timeout()
                logger.info("Reset timeout")
            else:
                logger.error("The service 'Reset Timeout' could not be sent.")
                QMessageBox.critical(
                    self, "Reset timeout error", "The service 'Reset Timeout' could not be sent.", QMessageBox.Close
                )

    @pyqtSlot()
    def refresh_timeout(self) -> None:
        """Refresh timeout"""
        if self.connected:
            data = self.elapsed_time_topic.get_data()
            if data is not None and data["valid_data"] == "new_data":
                self.time = int(self.timeout) - data["data"]
        else:
            self.time -= 1

        self.set_time_label()

    def set_time_label(self) -> None:
        """Set time to timeout_label"""
        # set color
        if self.time < 0:
            self.value_label.setStyleSheet("font:italic; color:red")
        elif self.time <= 300:
            self.value_label.setStyleSheet("font:italic; color:orange")
        else:
            self.value_label.setStyleSheet("font:italic; color:black")

        # set time
        self.value_label.setText(str(self.time))

    @pyqtSlot()
    def start_timer(self) -> None:
        """Start timer"""
        if not self.timer.isActive():
            self.set_timeout()
            self.timer.start(1000)

    def set_timeout(self) -> None:
        """Obtain timeout param from ROS param server."""
        ip = self.vehicle_info.get_vehicle_ip()
        name = self.vehicle_info.get_vehicle_namespace() + "/safety_supervisor/watchdog_timer/timeout"
        self.timeout = get_ros_param(ip, ROSBRIDGE_SERVER_PORT, name)["value"]

    @pyqtSlot()
    def connect(self) -> None:
        """Set connected state to True and start thread"""
        if not self.connected:
            self.connected = True
            worker = Worker(self.update_watchdog_status)
            self.threadpool.start(worker)

    def update_watchdog_status(self) -> None:
        """Susbscribe to watchdog topics"""
        try:
            self.subscribed = True
            watchdog_timer = self.topic_names["watchdog timer"]

            self.elapsed_time_topic = SubscribeToTopic(
                self.vehicle_info.get_vehicle_ip(),
                ROSBRIDGE_SERVER_PORT,
                self.vehicle_info.get_vehicle_namespace() + watchdog_timer,
            )

            self.elapsed_time_topic.subscribe()

            self.update_data_signal.emit()

        except OSError as oe:
            logger.exception(f"Reconnecting {oe}")
            self.disconnect_updates()
        except Exception as ex:
            logger.exception(f"Reconnecting {ex}")
            self.disconnect_updates()

        if not self.subscribed:
            QThread.sleep(5)
            self.connected = False
            self.reconnect_signal.emit()

    def disconnect_updates(self) -> None:
        """Disconnect updates, stops timer and close connexions"""
        self.subscribed = False
        self.timer.stop()

        if self.elapsed_time_topic is not None:
            self.elapsed_time_topic.close()
        self.elapsed_time_topic = None

        self.threadpool.clear()

    def closeEvent(self, a0: QCloseEvent) -> None:
        """Stop timer and close event"""
        self.disconnect_updates()
        self.time = 0
        super().closeEvent(a0)
