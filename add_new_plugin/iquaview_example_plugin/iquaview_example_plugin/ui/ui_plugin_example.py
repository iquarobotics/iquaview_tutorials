# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_plugin_example.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_PluginExample(object):
    def setupUi(self, PluginExample):
        PluginExample.setObjectName("PluginExample")
        PluginExample.resize(666, 108)
        self.gridLayout = QtWidgets.QGridLayout(PluginExample)
        self.gridLayout.setObjectName("gridLayout")
        self.gridLayout_2 = QtWidgets.QGridLayout()
        self.gridLayout_2.setSizeConstraint(QtWidgets.QLayout.SetDefaultConstraint)
        self.gridLayout_2.setObjectName("gridLayout_2")
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout_2.addItem(spacerItem, 1, 1, 1, 3)
        self.elapsed_time_label = QtWidgets.QLabel(PluginExample)
        self.elapsed_time_label.setObjectName("elapsed_time_label")
        self.gridLayout_2.addWidget(self.elapsed_time_label, 3, 1, 1, 1)
        self.reset_pushButton = QtWidgets.QPushButton(PluginExample)
        self.reset_pushButton.setObjectName("reset_pushButton")
        self.gridLayout_2.addWidget(self.reset_pushButton, 3, 3, 1, 1)
        self.value_label = QtWidgets.QLabel(PluginExample)
        self.value_label.setObjectName("value_label")
        self.gridLayout_2.addWidget(self.value_label, 3, 2, 1, 1)
        self.gridLayout.addLayout(self.gridLayout_2, 2, 0, 1, 2)
        self.toolbar = QtWidgets.QToolBar(PluginExample)
        self.toolbar.setIconSize(QtCore.QSize(16, 16))
        self.toolbar.setFloatable(False)
        self.toolbar.setObjectName("toolbar")
        self.gridLayout.addWidget(self.toolbar, 1, 0, 1, 2)
        self.status_toolbar = QtWidgets.QToolBar(PluginExample)
        self.status_toolbar.setIconSize(QtCore.QSize(16, 16))
        self.status_toolbar.setFloatable(False)
        self.status_toolbar.setObjectName("status_toolbar")
        self.gridLayout.addWidget(self.status_toolbar, 6, 0, 1, 2)
        spacerItem1 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout.addItem(spacerItem1, 5, 0, 1, 2)

        self.retranslateUi(PluginExample)
        QtCore.QMetaObject.connectSlotsByName(PluginExample)

    def retranslateUi(self, PluginExample):
        _translate = QtCore.QCoreApplication.translate
        PluginExample.setWindowTitle(_translate("PluginExample", "Example of a plugin"))
        self.elapsed_time_label.setText(_translate("PluginExample", "Time:"))
        self.reset_pushButton.setText(_translate("PluginExample", "Reset "))
        self.value_label.setText(_translate("PluginExample", "0"))
        self.toolbar.setWindowTitle(_translate("PluginExample", "toolBar"))
        self.status_toolbar.setWindowTitle(_translate("PluginExample", "status_toolBar"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    PluginExample = QtWidgets.QDialog()
    ui = Ui_PluginExample()
    ui.setupUi(PluginExample)
    PluginExample.show()
    sys.exit(app.exec_())
