# Copyright (c) 2022 Iqua Robotics SL - All Rights Reserved
#
# This file is subject to the terms and conditions defined in file
# 'LICENSE.txt', which is part of this source code package.

"""
Template for creating a plugin module
"""

from iquaview_lib.baseclasses.pluginbase import PluginBase
from iquaview_lib.vehicle.vehicleinfo import VehicleInfo
from PyQt5.QtCore import pyqtSlot
from PyQt5.QtWidgets import QAction, QMenuBar

import iquaview_example_plugin


class ExamplePluginModule(PluginBase):
    """Plugin module class"""

    def __init__(self, **kwargs) -> None:
        """
        Module constructor
        :param kwargs: is a dictionary composed of
                       {
                         proj: QgsProject,
                         canvas: QgsMapCanvas,
                         config: Config,
                         vehicle_info: VehicleInfo,
                         vehicle_data: VehicleData,
                         vehicle_status: VehicleStatus,
                         plugin_bar: QMenuBar,
                         view_menu_toolbar: QMenu,
                         entities_manager: EntitiesManager
                         parent: QMainWindow
                       }
        """
        # set name and parent
        super().__init__(iquaview_example_plugin.PLUGIN_NAME, kwargs["parent"])
        if self.module_found:
            # get plugin menubar
            self.menubar: QMenuBar = kwargs["plugin_bar"]
            # get plugin vehicle info
            vehicle_info: VehicleInfo = kwargs["vehicle_info"]

            # create action to call the plugin
            self.example_plugin_action = QAction("Example Plugin", self)

            # add action to menubar
            self.menubar.addActions(
                [
                    self.example_plugin_action,
                ]
            )

            # connect the action trigger to function slot
            # function will be called every time action is triggered
            self.example_plugin_action.triggered.connect(self.open_example_plugin)

            # Create plugin dialog
            self.example_plugin_dlg = iquaview_example_plugin.ExamplePluginDlg(vehicle_info)

    @pyqtSlot()
    def open_example_plugin(self) -> None:
        """Open iquaview example dialog"""
        self.example_plugin_dlg.show()
        self.example_plugin_dlg.connect()

    @staticmethod
    def version() -> str:
        """Return version of plugin"""
        return f"{iquaview_example_plugin.PLUGIN_NAME} {iquaview_example_plugin.__version__}"

    def disconnect_module(self) -> None:
        """Close dialog"""
        self.example_plugin_dlg.close()

    def remove(self) -> None:
        """Removes module from iquaview"""
        self.disconnect_module()
        self.menubar.removeAction(self.example_plugin_action)
