# Copyright (c) 2019 Iqua Robotics SL - All Rights Reserved
#
# This file is subject to the terms and conditions defined in file
# 'LICENSE.txt', which is part of this source code package.

from .exampleplugin import ExamplePluginDlg

# To be able to call the plugin from iquaview it is necessary to import the module here
from .module import ExamplePluginModule

# define plugin version
version_info = (24, 1, 0)

PLUGIN_NAME = "iquaview_example_plugin"

__version__ = ".".join(map(str, version_info))
