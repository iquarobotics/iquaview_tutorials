# Copyright (c) 2019 Iqua Robotics SL - All Rights Reserved
#
# This file is subject to the terms and conditions defined in file
# 'LICENSE.txt', which is part of this source code package.

import os
import sys

from PyQt5 import QtWidgets

from iquaview_example_plugin.exampleplugin import ExamplePluginDlg
from iquaview_lib import resources_rc  # pylint: disable=unused-import
from iquaview_lib.config import Config
from iquaview_lib.vehicle.vehicleinfo import VehicleInfo

"""Simple main to call plugin as standalone outside of IQUAview"""

if __name__ == "__main__":
    # Create qApplication
    app = QtWidgets.QApplication(sys.argv)
    # Read configs
    config = Config()
    config.load()

    xml_filename = "{}".format(os.path.abspath(os.path.expanduser(config.settings["last_auv_config_xml"])))
    vehicle_info = VehicleInfo(xml_filename)
    example_plugin_dlg = ExamplePluginDlg(vehicle_info)
    example_plugin_dlg.show()
    sys.exit(app.exec_())
