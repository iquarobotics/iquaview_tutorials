# IQUAview Example Plugin

[TOC]

## Scope and prerequisites

This tutorial/example aims to introduce an IQUAview user to its plugin system. Anybody following this tutorial should already be familiar with:

* Cola2 software architecture, being able to start a simulation environment.
* IQUAview as an end-user.
* Python programming.

This tutorial will also provide some insights on PyQt, however, it is not a tutorial on PyQt.

## Plugin files and classes structure

Any IQUAview plugin folder has the following folder and files structure:

```none
plugin_name
|   CHANGELOG.md
|   LICENSE.txt
|   MANIFEST.in
|   README.md
|   setup.py
+-- plugin_name
    |   __init__.py
    |   __main__.py (optional)
    |   configuration.xml (optional)
    |   module.py
    |   file_1.py
    |   file_2.py
    |   ....
    +-- ui (optional)
        |   __init__.py
        |   ui_1.ui
        |   ui_1.py
        |   ui_2.ui
        |   ui_2.py
        |   ...
```

The files `CHANGELOG.md`, `LICENSE.txt` and `README.md` are standard files for software packages and are not explained within this tutorial. The rest of the files are going to be reviewed with the provided code in the `iquaview_example_plugin`, within this repository, in the following sections.

## Setup and Manifest

One of the most basic features, is to be able to install the developed plugins. IQUAview and its plugins, use standard `pip3` tool for installation. To prepare the software package, the files `MANIFEST.in` and `setup.py` are required:

* The `MANIFEST.in` file consists of commands, one per line, instructing setup tools to add or remove some set of files from the sdist. More information can be found [here](https://setuptools.pypa.io/en/latest/userguide/miscellaneous.html).

* The `setup.py` python file  is used for [setuptools](https://setuptools.pypa.io/en/latest/index.html) to install the python module/package. Installing the plugin is mandatory for IQUAview to be able to detect it and load it. This python file sets up the package using the [`setuptools.setup`](https://setuptools.pypa.io/en/latest/userguide/quickstart.html) function. In the provided examples the function is called with the following parameters:

    * `name`: Name of the plugin.
    * `version`: Version of the plugin.
    * `description`: Description of the plugin.
    * `author`: Author of the plugin
    * `author_email`: Email of the author of the plugin.
    * `packages`: List of python packages to process. In general this will list the name (not the path) of any folder with python code inside. For further information go [here](https://setuptools.pypa.io/en/latest/userguide/package_discovery.html).
    * `include_package_data`: Set to true to include package data (i.e. non-python files) or false otherwise.
    * `package_data`: List of data files that goes with the plugin if the `include-package_data` is set to `True`.

## Plugin folder

The folder `plugin_name/plugin_name` is the main folder for the plugin code (in this case is `iquaview_example_plugin/iquaview_example_plugin`). In this folder the python package is organized as a standard python package. It normally contains at least three necessary files: `__init__.py`, `module.py` and `file_1.py`. Apart from these necessary files, it can contain optional files such as `configuration.xml` (which is used to define things such as services to call, topics to subscribe...) or an `ui` folder with user interfaces designed with Qt Designer.

### The `__init__.py` file

This file does the import of the different python modules in the library as well as defining the version information and plugin name. The provided example first imports the plugin module as well as the plugin GUI:

```python
from .exampleplugin import ExamplePluginDlg
from .module import ExamplePluginModule
```

and then defines the version and plugin name:

```python
version_info = (24, 1, 0)
PLUGIN_NAME = "iquaview_example_plugin"
__version__ = ".".join(map(str, version_info))
```

### The `configuration.xml` file

The plugin xml file contains data of the plugin that can vary over time such as topic names, services names, etc. These files can be used along with [`VehicleDataHandler`](https://bitbucket.org/iquarobotics/iquaview_lib/src/master/iquaview_lib/xmlconfighandler/vehicledatahandler.py) where the basic tags for the vehicle information can be retrieved and parsed as it will be later explained in this tutorial. However, they can also include other data such as sensor-specific data. In this case, the class [`XMLConfigParser`](https://bitbucket.org/iquarobotics/iquaview_lib/src/master/iquaview_lib/xmlconfighandler/xmlconfigparser.py) can be used to parse any kind of xml tags.

### The `module.py` file

This file is the one that allows IQUAview via its [plugin manager](https://bitbucket.org/iquarobotics/iquaview/src/master/iquaview/src/plugins/pluginmanager.py) to import all the installed plugins. This is the class that has to inherit from [PluginBase class in iquaview_lib](https://bitbucket.org/iquarobotics/iquaview_lib/src/master/iquaview_lib/baseclasses/pluginbase.py). The plugin manager will import any class that inherits from the plugin base class that has a different class name from any other already imported plugin class. For example, if two different plugins have the same name for the plugin module class, the manager will only import one of the two.

Another very important function of this class, is locating the plugin in the right places of IQUAview (menus, toolbars....) as well as initializing the plugin GUI and connections. The information from IQUAview is passed on to the plugin using the `**kwargs` parameter on the class module constructor. This dictionary contains the following:

| Dictionary name    | Class type                                                                                                             | Description                                                                                                                                                       |
|-------------------|------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| proj              | [`QgsProject`](https://qgis.org/pyqgis/3.0/core/Project/QgsProject.html)                                               | QGS project of IQUAview                                                                                                                                           |
| canvas            | [`QgsMapCanvas`](https://qgis.org/pyqgis/3.0/gui/Map/QgsMapCanvas.html?highlight=qgsmapcanvas)                         | The canvas (main central display) of IQUAview                                                                                                                     |
| config            | [`Config`](https://bitbucket.org/iquarobotics/iquaview_lib/src/master/iquaview_lib/config.py)                          | IQUAview configuration class                                                                                                                                      |
| vehicle_info      | [`VehicleInfo`](https://bitbucket.org/iquarobotics/iquaview_lib/src/master/iquaview_lib/vehicle/vehicleinfo.py)        | Class to read and store the xml structure associated to the vehicle_info tag in the AUV config file.                                                              |
| vehicle_data      | [`VehicleData`](https://bitbucket.org/iquarobotics/iquaview_lib/src/master/iquaview_lib/vehicle/vehicledata.py)        | Class with the data from the subscribed topics in the main GUI. These are the topics configured in the _Customization Settings_ section of the AUV configuration. |
| vehicle_status    | [`VehicleStatus`](https://bitbucket.org/iquarobotics/iquaview_lib/src/master/iquaview_lib/vehicle/vehiclestatus.py)    | Vehicle status information.                                                                                                                                       |
| plugin_bar        | [`QMenuBar`](https://doc.qt.io/qt-5/qmenubar.html)                                             | IQUAview main window's menu bar at the top of the GUI.                                                                                                            |
| view_menu_toolbar | [`QMenu`](https://doc.qt.io/qt-5/qmenu.html)                                        | _Toolbars_ menu in the main GUI under _View_ menu.                                                                                                                |
| entities_manager  | [`EntitiesManager`](https://bitbucket.org/iquarobotics/iquaview_lib/src/master/iquaview_lib/entity/entitiesmanager.py) | Manger of the tracks in the tracks panel and the main canvas.                                                                                                     |
| parent            | [`QMainWindow`](https://doc.qt.io/qt-5/qmainwindow.html)                                    | Main IQUAview window                                                                                                                                              |

The plugin module can access any of these objects through its constructor:

```python
class ExamplePluginModule(PluginBase):
    def __init__(self, **kwargs) -> None:
        entities_manager = kwargs["entities_manager"]
```

#### The `module.py` explained

The provided example first initializes the parent class which provides if the module is correctly installed though `self.module_found`. If the module is found, it retrieves the relevant information from the `kwargs` constructor variable:

```python
super().__init__(iquaview_example_plugin.PLUGIN_NAME, kwargs["parent"])
if self.module_found:
    # get plugin menubar
    self.menubar: QMenuBar = kwargs["plugin_bar"]
    # get plugin vehicle info
    vehicle_info: VehicleInfo = kwargs["vehicle_info"]
```

Then, it continues by creating a `QAction` to add to the IQUAview plugin menu so that this plugin dialog can be opened:

```python
# create action to call the plugin
self.example_plugin_action = QAction(
    "Example Plugin",
    self)

# add action to menubar
self.menubar.addAction(
    [
        self.example_plugin_action,
    ]
)
```

It connects the action trigger signal to a class method slot to show the dialog:

```python
# connect the action trigger to function slot
# function will be called every time action is triggered
self.example_plugin_action.triggered.connect(self.open_example_plugin)
```

and finally it creates the actual dialog that will display the data:

```python
self.example_plugin_dlg = iquaview_example_plugin.ExamplePluginDlg(vehicle_info)
```

The callback function for the example plugin action shows the dialog and cals its `connect` function:

```python
@pyqtSlot()
def open_example_plugin(self) -> None:
    """Open iquaview example dialog"""
    self.example_plugin_dlg.show()
    self.example_plugin_dlg.connect()
```

The version function is used from the IQUAview plugin manager to add the plugin information to the table and composes the plugin name with its version:

```python
@staticmethod
def version() -> str:
    """Return version of plugin"""
    return f"{iquaview_example_plugin.PLUGIN_NAME} {iquaview_example_plugin.__version__}"
```

The last two functions are the ones that handle the closing of the dialog and removing it from the menu bar when the plugin is removed/uninstalled.

```python
def disconnect_module(self) -> None:
    """Close dialog"""
    self.example_plugin_dlg.close()

def remove(self):
    """Removes module from iquaview"""
    self.disconnect_module()
    self.menubar.removeAction(self.example_plugin_action)
```

### The `file_X.py` files

These files (`file_1.py`, `file_2.py`...) are the actual widgets and objects that implement the functionalities of the plugin. As any Qt widget, it can depend on a Qt Designer ui file `ui_X.ui` (see [Using the Qt Designer for UI interfaces](#using-the-qt-designer-for-ui-interfaces) for further details). In the case of this example, the goal is to interact with the vehicle watchdog timeout (get the current value, change the parameter and reset the timeout).

#### The `exampleplugin.py` explained

The example plugin defines two Qt signals to interact with Qt widgets and objects:

```python
class ExamplePluginDlg(QDialog, Ui_PluginExample):
    """Example plugin dialog"""
    update_data_signal = pyqtSignal()
    reconnect_signal = pyqtSignal()
```

Then, the constructor initializes the base class and sets up the user interface designed using the Qt designer:

```python
    def __init__(self, vehicle_info, parent=None):
        super().__init__(parent)
        self.setupUi(self)
```

Next, the constructor initializes some variables and reads the configuration xml file that will be later used to access configurable information (services, topics...) of the plugin:

```python
        self.connected = False
        self.config_name = "example_plugin_configuration.xml"
        self.configfile = os.path.join(os.path.dirname(os.path.abspath(__file__)), self.config_name)
```

Then, more variables and timers are initialized

```python
        self.elapsed_time_topic: Optional[SubscribeToTopic] = None
        self.subscribed: bool = False
        self.timer = QTimer()
        self.timeout: int = 0
        self.time: int = 0
        self.threadpool = QThreadPool()
        self.vehicle_info = vehicle_info
```

The next step in the constructor is to read the data from the plugin xml

```python
        # load configuration
        self.topic_names: Dict[str, str] = {}
        self.services_names: Dict[str, str] = {}
        self.load_configuration()

        ...

    def load_configuration(self) -> None:
        # create vehicle data handler
        vd_handler = VehicleDataHandler(self.configfile)

        # get vehicle data topics from xml
        xml_vehicle_data_topics = vd_handler.read_topics()
        for topic in xml_vehicle_data_topics:
            self.topic_names[topic.get("id")] = topic.text

        # get vehicle data services from xml
        xml_vehicle_data_services = vd_handler.read_services()
        for service in xml_vehicle_data_services:
            self.services_names[service.get("id")] = service.text
```

One of the goals of the plugin is to configure the watchdog timer. For this, first a `QAction` is created and added to the ui toolbar. The `QAction` will be shown in the toolbar as the icon loaded from the resources file (see [Adding icons to the interface](#adding-icons-to-the-interface) for further resources information):

```python
        self.configuration_action = QAction(
            QIcon(":/resources/mActionAUVConfigParameters.svg"),
            "Config Parameters",
            self,
        )
        self.toolbar.addAction(self.configuration_action)
```

Finally, the constructor connects the different signals and slots from the different `QObjects`:

```python
        # connect signals to slots
        self.configuration_action.triggered.connect(self.open_configuration)
        self.reset_pushButton.clicked.connect(self.reset_timeout)
        self.timer.timeout.connect(self.refresh_timeout)
        self.update_data_signal.connect(self.start_timer)
        self.reconnect_signal.connect(self.connect)
```

Where the `self.reset_pushButton` was defined in the ui file and the `self.update_data_signal` and `self.reconnect_signal` are defined in this same class as all the slots connected to these signals.

The `open_configuration` function is called and a dialog is shown when the action `self.configuration_action` is triggered. This functions creates an `AUVConfigParamsDlg`:

```python
    @pyqtSlot()
    def open_configuration(self):
        """Open configuration"""
        acpd = AUVConfigParamsDlg(self.configfile, self.vehicle_info, unique_section=True)
```

Then the safety section of the plugin configuration is loaded and the dialog is executed showing a message box if any has risen an exception:

```python
        try:
            acpd.load_section("Safety")
            acpd.exec_()

        except Exception as ex:
            logger.error("Connection with COLA2 could not be established")
            QMessageBox.critical(
                self,
                "Connection with AUV Failed",
                f"{ex} Connection with COLA2 could not be established",
                QMessageBox.Close,
            )
```

Inspecting the `example_plugin_configuration.xml`, the loaded section is the following:

```xml
    <ros_params>
        <section>
            <description>Safety</description>
            <param>
                <description>Watchdog timeout (seconds)</description>
                <action_id>/safety_supervisor/reload_params</action_id>
                <field>
                    <field_name>/safety_supervisor/watchdog_timer/timeout</field_name>
                    <field_type>double</field_type>
                </field>
            </param>
        </section>
    </ros_params>
```

This section describes the watchdog timeout.

* `action_id` is the ROS service to call (without the robot namespace as it is added automatically from the vehicle information) to make the safety supervisor node reload the parameters from the parameters server.
* `field` describes a specific parameter with parameter name `field_name` and type `filed_type`.

The QPushButton of the ui (created with the Qt Designer) is responsible for calling the service that restarts the robot's watchdog timeout. This function, when `self.connected` is true will first retrieve the service from the list of services read from the plugin's xml configuration file:

```python
    @pyqtSlot()
    def reset_timeout(self):
        """Send service that reset timeout"""
        if self.connected:
            reset_timeout = self.services_names.get("reset timeout")
```

If the reset timeout service name is recovered successfully, the service is called and the result is checked to show a message box if the service call was not successful:

```python
            if reset_timeout is not None:
                response = send_trigger_service(
                    self.vehicle_info.get_vehicle_ip(),
                    ROSBRIDGE_SERVER_PORT,
                    self.vehicle_info.get_vehicle_namespace() + reset_timeout,
                )
                if not response["values"]["success"]:
                    QMessageBox.critical(self,
                                         "Reset timeout failed",
                                         response["values"]["message"],
                                         QMessageBox.Close)
```

Finally the `set_timeout` function (explained below) is called and the logger is used to log information.

The `refresh_timeout` function is called every time the `QTimer` of `self.timer` times out (once a second as we will see later). The function checks if the widget is connected and if so, retrieves the data from the watchdog topic. If all the data is correct, it computes and saves the remaining time before the timeout occurs. Finally, the label displaying the time is updated with the `self.set_time_label()` function:

```python
    @pyqtSlot()
    def refresh_timeout(self):
        """Refresh timeout"""
        if self.connected:
            data = self.elapsed_time_topic.get_data()
            if data is not None and data["valid_data"] == "new_data":
                self.time = int(self.timeout) - data["data"]
        else:
            self.time -= 1

        self.set_time_label()
```

The `set_time_label` function changes the font and color of the displayed timeout according to some thresholds and sets the display text to the remaining timeout time:

```python
    def set_time_label(self):
        """Set time to timeout_label"""
        # set color
        if self.time < 0:
            self.value_label.setStyleSheet('font:italic; color:red')
        elif self.time <= 300:
            self.value_label.setStyleSheet('font:italic; color:orange')
        else:
            self.value_label.setStyleSheet('font:italic; color:black')

        # set time
        self.value_label.setText(str(self.time))
```

Whenever the `update_data_signal` is emitted, the `start_timer` function is called and the timer, if not already started, is started with a period of 1000 ms after reading the timeout from the vehicle with `self.set_timeout()`:

```python
    @pyqtSlot()
    def start_timer(self):
        """Start timer"""
        if not self.timer.isActive():
            self.set_timeout()
            self.timer.start(1000)
```

The function `set_timeout` uses the function `get_ros_param` from `iquaview_lib.cola2_api` to retrieve the value of the total timeout from the parameter server through the rosbridge:

```python
    def set_timeout(self):
        """Obtain timeout param from ROS param server."""
        ip = self.vehicle_info.get_vehicle_ip()
        name = self.vehicle_info.get_vehicle_namespace() + '/safety_supervisor/watchdog_timer/timeout'
        self.timeout = (get_ros_param(ip, ROSBRIDGE_SERVER_PORT, name)['value'])
```

Whenever the `reconnect_signal` is emitted, the `connect` function is called. This function starts a new thread to retrieve the data from the topics without blocking the GUI main thread:

```python
    @pyqtSlot()
    def connect(self):
        """Set connected state to True and start thread"""
        if not self.connected:
            self.connected = True
            worker = Worker(self.update_watchdog_status)
            self.threadpool.start(worker)
```

This function creates a subscriber to the topics over the rosbridge. Since this connection might take some time, this function, when called in the `connect` function, is executed on a separate thread to avoid blocking the main GUI thread. The topic subscription uses the [`SubscribeToTopic`](https://bitbucket.org/iquarobotics/iquaview_lib/src/main/iquaview_lib/cola2api/cola2_interface.py) class to connect to the rosbridge server on the robot and takes as inputs the vehicle ip, the rosbridge port and the topic to subscribe to:

```python
    def update_watchdog_status(self):
        """Subscribe to watchdog topics"""
        try:
            self.subscribed = True
            watchdog_timer = self.topic_names["watchdog timer"]

            self.elapsed_time_topic = SubscribeToTopic(
                self.vehicle_info.get_vehicle_ip(),
                ROSBRIDGE_SERVER_PORT,
                self.vehicle_info.get_vehicle_namespace() + watchdog_timer,
            )

            self.elapsed_time_topic.subscribe()

            self.update_data_signal.emit()

        except OSError as oe:
            logger.exception(f"Reconnecting {oe}")
            self.disconnect_updates()
        except Exception as ex:
            logger.exception(f"Reconnecting {ex}")
            self.disconnect_updates()

        if not self.subscribed:
            QThread.sleep(5)
            self.connected = False
            self.reconnect_signal.emit()
```

The `disconnect_updates` function stops the timer for the `refresh_timeout` function, stops the subscription to the topic through the rosbridge and clears the threadpool with the worker used for subscription:

```python
    def disconnect_updates(self):
        """Disconnect updates, stops timer and close connexions"""
        self.subscribed = False
        self.timer.stop()

        if self.elapsed_time_topic is not None:
            self.elapsed_time_topic.close()
        self.elapsed_time_topic = None

        self.threadpool.clear()
```

Finally, the [`closeEvent`](https://doc.qt.io/qtforpython-5/PySide2/QtWidgets/QWidget.html#PySide2.QtWidgets.PySide2.QtWidgets.QWidget.closeEvent) overloads the base class implementation of what to do when the widget is closed. This overload makes sure that when the widget is closed everything is disconnected using the `disconnect_updates` function and resets the time to 0 before calling the base class `closeEvent` implementation:

```python
    def closeEvent(self, event: QCloseEvent) -> None:
        """Stop timer and close event"""
        self.disconnect_updates()
        self.time = 0
        super().closeEvent(event)
```

## Qt FAQS

### Using the Qt Designer for UI interfaces

[Qt Designer](https://doc.qt.io/qt-5/qtdesigner-manual.html) is a GUI provided by The Qt Company to ease the design and develop of GUIs. If a GUI is designed using this tool, a `.ui` file is created with the GUI content. This file has to be converted to a python file in the following way:

```bash
pyuic5 -x ui_plugin.ui -o ui_plugin.py
```

To use `pyuic5` install the package pyqt5-dev-tools `sudo apt install pyqt5-dev-tools`.

Once this file is created, import it to your python class as follows:

```python
from PyQt5.QtWidgets import QWidget
from path_to_my_ui_python_file import Ui_PluginExample

class MyQtGUIBasedOnUI(QWidget, Ui_MyQtGUIBasedOnUi):

    def __init__(self, parent=None):
        super().__init__(parent)
        self.setupUi(self)
```

After initializing the class in this way, you can access any of the widgets of the GUI with the `objectName` property given in the designer property editor. For example if the GUI has a label named `label_1` it can be accessed with `self.label_1`.

### Adding icons to the interface

[Qt resource system](https://doc.qt.io/qt-5/resources.html) is a powerful mechanism to incorporate resources to your application. If you need any external information during your program execution (icons, user manuals....) these can be added to a resource file such as this:

```xml
<RCC>
    <qresource prefix="/">
        <file>images/my_image.png</file>
    </qresource>
</RCC>
```

where the `<file>` tag points to the relative path of the file `my_image.png` with respect to the resource xml file. Before using these resources in the code, this file has to be converted to a python file:

```bash
pyrcc5 resources.qrc -o resources_rc.py
```

then, this resource file can be imported to your python scripts and use the resources the following way:

```python
from PyQt5.QtGui import QIcon
from path_to_my_resources_file import resources_rc
QIcon(":/images/my_image.png")
```
